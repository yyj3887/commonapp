package com.kr.common.app;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class TestController {
	String strCR = "\n";

	@Autowired
	SqlSessionFactory sqlSessionFactory;

//	@GetMapping("selectIntegratedMaster")
//	public ResponseEntity<List<Map<String, Object>>> SelectIntegratedMaster(String txtCodeValue) {
//
//		Map<String, Object> param = new HashMap<>();
//		List<Map<String, Object>> itgMarsterDTOlist = null;
//
//		try (SqlSession session = sqlSessionFactory.openSession()) {
//			if (!txtCodeValue.trim().isEmpty()) {
//				param.put("codetype_id", txtCodeValue);
//			}
//
//			itgMarsterDTOlist = session.selectList("aop.sqlmap.aopSQLMapper.selectITGMaster", param); // 1
//		} catch (Exception e) {
//			// TODO: handle exception
//			return new ResponseEntity<List<Map<String, Object>>>(itgMarsterDTOlist, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//		return new ResponseEntity<List<Map<String, Object>>>(itgMarsterDTOlist, HttpStatus.OK);
//	}
	
	@GetMapping("call")
	public ResponseEntity<List<Map<String, Object>>> SelectIntegratedMaster() {
		Map<String, Object> param = new HashMap<>();
		List<Map<String, Object>> result = null;
		try (SqlSession session = sqlSessionFactory.openSession()) {
			
			result = session.selectList("com.kr.common.app.test", param); // 1
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<List<Map<String, Object>>>(result, HttpStatus.INTERNAL_SERVER_ERROR);
		}		
		return new ResponseEntity<List<Map<String, Object>>>(result, HttpStatus.OK);
	}

}
